def leer_datos(nombre_fichero):
    with open(nombre_fichero, 'r') as descriptor_fichero:
        for linea in descriptor_fichero:
          lista = linea.split(' ')
          lista_letras = []
          for letra in lista:
              lista_letras.append(letra)
    return lista_letras
            
            
if __name__ == "__main__" :
    data = leer_datos('salida-ej3.txt')